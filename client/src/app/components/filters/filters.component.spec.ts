import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FiltersComponent } from './filters.component';
import { JobsService } from '../../services/jobs.service';

import { JOB_STATE_ACTIVE, JOB_STATE_INACTIVE } from '../../common/constants';

fdescribe('FiltersComponent', () => {

  let component: FiltersComponent;
  let fixture: ComponentFixture<FiltersComponent>;
  let jobsService: JobsService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        FiltersComponent
      ],
      providers: [
        JobsService
      ]
    });

    fixture = TestBed.createComponent(FiltersComponent);
    component = fixture.componentInstance;
    jobsService = TestBed.inject(JobsService);

    fixture.whenStable();

    spyOn(component.toggleFilter, 'emit');

    fixture.detectChanges();
  }));

  it('should create the filters', () => {
    fixture = TestBed.createComponent(FiltersComponent);
    expect(component).toBeTruthy();
  });

  it(`should have activate/deactivate filters`, async () => {
    expect(component.toggleFilter.emit).not.toHaveBeenCalled();
    component.clickFilter(JOB_STATE_ACTIVE);
    expect(component.toggleFilter.emit).toHaveBeenCalled();
  });
});
