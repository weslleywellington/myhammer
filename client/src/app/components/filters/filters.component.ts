import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { JobsService } from 'src/app/services/jobs.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  @Output()
  toggleFilter = new EventEmitter();

  constructor(public jobsService: JobsService ) { }

  ngOnInit(): void {
  }

  clickFilter(value): void {
    if (this.toggleFilter) {
      this.toggleFilter.emit(value);
    }
  }
}
