import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { JobDetailComponent } from './job-detail.component';
import { JobsService } from '../../services/jobs.service';

describe('JobDetailComponent', () => {

  let component: JobDetailComponent;
  let fixture: ComponentFixture<JobDetailComponent>;
  let jobsService: JobsService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        JobDetailComponent
      ],
      providers: [
        JobsService
      ]
    });

    fixture = TestBed.createComponent(JobDetailComponent);
    component = fixture.componentInstance;
    jobsService = TestBed.inject(JobsService);

    fixture.whenStable();

    spyOn(component.closeJob, 'emit');

    fixture.detectChanges();
  }));

  it('should create the job detail', () => {
    fixture = TestBed.createComponent(JobDetailComponent);
    expect(component).toBeTruthy();
  });

  it(`should have clicked in close job`, async () => {

    expect(component.closeJob.emit).not.toHaveBeenCalled();

    component.clickClose();

    expect(component.closeJob.emit).toHaveBeenCalled();
  });
});
