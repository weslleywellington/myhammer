import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { JobsService } from 'src/app/services/jobs.service';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss']
})
export class JobDetailComponent implements OnInit {

  @Output()
  closeJob = new EventEmitter();

  constructor(public jobsService: JobsService ) { }

  ngOnInit(): void { }

  clickClose(): void {
    if (this.closeJob) {
      this.closeJob.emit();
    }
  }
}
