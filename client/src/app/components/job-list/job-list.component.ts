import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { JobsService } from 'src/app/services/jobs.service';


@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {

  @Input()
  jobs = [];

  @Output()
  openJob = new EventEmitter();

  constructor(public jobsService: JobsService ) { }

  ngOnInit(): void { }

  clickJob(job): void {
    if (this.openJob) {
      this.openJob.emit(job);
    }
  }
}
