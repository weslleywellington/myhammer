import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { JobListComponent } from './job-list.component';
import { JobsService } from '../../services/jobs.service';

import { JOB_STATE_ACTIVE } from '../../common/constants';

describe('JobListComponent', () => {

  let component: JobListComponent;
  let fixture: ComponentFixture<JobListComponent>;
  let jobsService: JobsService;

  const job = {
    id: '12341234',
    state: JOB_STATE_ACTIVE,
  };

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        JobListComponent
      ],
      providers: [
        JobsService
      ]
    });

    fixture = TestBed.createComponent(JobListComponent);
    component = fixture.componentInstance;
    jobsService = TestBed.inject(JobsService);

    fixture.whenStable();

    spyOn(component.openJob, 'emit');

    fixture.detectChanges();
  }));

  it('should create the job list', () => {
    fixture = TestBed.createComponent(JobListComponent);
    expect(component).toBeTruthy();
  });

  it(`should have clicked in open job`, async () => {

    expect(component.openJob.emit).not.toHaveBeenCalled();

    component.clickJob(job);

    expect(component.openJob.emit).toHaveBeenCalled();
  });
});
