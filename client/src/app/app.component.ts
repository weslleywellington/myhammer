import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { JobsService } from './services/jobs.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private jobs: Array<any>;
  jobsFiltered: Array<any>;

  constructor(public jobsService: JobsService, private cdr: ChangeDetectorRef ) { }

  ngOnInit() {
    // Initialize Jobs
    this.jobsService.fetchAll().subscribe((jobs: Array<any>) => {
      this.jobs = jobs;
      this.updateJobs();
    });
  }

  updateJobs() {
    const filters = Object.keys(this.jobsService.filters).filter((criteria) => this.jobsService.filters[criteria]);
    this.jobsFiltered = this.jobs.filter((job) => filters.includes(job.state));

    // Set the first as active
    this.openJob(this.jobsFiltered[0]);
    this.cdr.markForCheck();
  }

  openJob(job) {
    if (job && job.id) {
      this.jobsService.fetchById(job.id).subscribe((res: any) => {
        this.jobsService.selectedJob = res;
      });
    }
  }

  closeJob() {
    this.jobsService.selectedJob = undefined;
  }

  toggleFilter(filter) {
    this.closeJob();
    this.jobsService.filters[filter] = !this.jobsService.filters[filter];
    this.updateJobs();
  }
}
