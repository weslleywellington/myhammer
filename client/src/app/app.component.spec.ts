import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { JobsService } from './services/jobs.service';
import { of } from 'rxjs';

import { JOB_STATE_ACTIVE, JOB_STATE_INACTIVE } from './common/constants';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let jobsService: JobsService;

  const jobId = '1111';
  const jobs = [
    { id: jobId, state: JOB_STATE_ACTIVE },
    { id: '2222', state: JOB_STATE_INACTIVE },
    { id: '3333', state: JOB_STATE_ACTIVE },
  ];

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        JobsService
      ]
    });

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    jobsService = TestBed.inject(JobsService);

    fixture.whenStable();

    spyOn(jobsService, 'fetchAll').and.returnValue(of(jobs));
    spyOn(jobsService, 'fetchById').and.returnValue(of(jobs.filter((job) => job.id === jobId)));

    fixture.detectChanges();
  }));

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    expect(component).toBeTruthy();
  });

  it(`should have initialized`, () => {
    expect(jobsService.fetchAll).toHaveBeenCalled();
    expect(jobsService.fetchById).toHaveBeenCalled();

    // tslint:disable-next-line: no-string-literal
    expect(component['jobs']).toEqual(jobs);
    expect(jobsService.selectedJob).toEqual([jobs[0]]);
  });


  it(`should have updated Jobs`, () => {
    expect(jobsService.fetchAll).toHaveBeenCalled();
    expect(component.jobsFiltered).toEqual(jobs.filter(job => job.state === JOB_STATE_ACTIVE));
  });

  it(`should have opened Correct Job`, async () => {
    expect(jobsService.fetchById).toHaveBeenCalled();
    expect(jobsService.selectedJob).toEqual([jobs[0]]);
  });

  it(`should have closed Job`, async () => {
    component.closeJob();
    expect(jobsService.selectedJob).toEqual(undefined);
  });


  it(`should have toggled Filters`, async () => {
    expect(jobsService.filters).toEqual({
      active: true,
      inactive: false,
    });

    component.toggleFilter(JOB_STATE_INACTIVE);

    // tslint:disable-next-line: no-string-literal
    expect(component['jobs'].length).toEqual(3);

    expect(jobsService.filters).toEqual({
      active: true,
      inactive: true,
    });
  });
});
