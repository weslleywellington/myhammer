import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  private jobsAPI = 'http://localhost:5000/api/v1/jobs';
  selectedJob: any;
  filters = {
    active: true,
    inactive: false,
  };

  constructor() { }

  private getToken() {
    const token = 'h78sadh87ady82h239789893';
    const headers = {
      'Content-Type': 'application/json',
      // tslint:disable-next-line: object-literal-key-quotes
      'Authorization': `Bearer ${ token }`,
    };

    return {headers};
  }

  fetchAll() {
    return new Observable(observer => {
      fetch(this.jobsAPI, this.getToken())
        .then(res => {
          return res.json();
        })
        .then(body => {
          observer.next(body);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
  }

  fetchById(jobId) {
    if (!jobId) {
      return of(null);
    }

    return new Observable(observer => {
      fetch(`${ this.jobsAPI }/${jobId}`, this.getToken())
        .then(res => {
          return res.json();
        })
        .then(body => {
          observer.next(body);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
  }
}
