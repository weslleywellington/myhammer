const notFound = (req, res, next) => {
  res.status(404)
  const error = new Error(`Not Found - ${ req.originalUrl }`)
  next(error)
}

/* eslint-disable no-unused-vars */
const errorHandler = (err, req, res, next) => {
  /* eslint-enable no-unused-vars */
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500
  res.status(statusCode)
  res.json({
    message: err.message,
    stack: process.env.NODE_ENV === 'production' ? undefined : err.stack
  })
}

const getBearerToken = (req) => {
  if (!req.headers || !req.headers.authorization) return
  
  const [ type, token ] = req.headers.authorization.split(' ')

  if (type.toLowerCase() === 'bearer') return token
}

const authorization = (req, res, next) => {
  const token = getBearerToken(req)

  if (!token) {
    res.status(403)
    const error = new Error(`FORBIDDEN`)
    next(error)
  } else {
    return next()
  }
}

module.exports = {
  notFound,
  errorHandler,
  authorization
}
