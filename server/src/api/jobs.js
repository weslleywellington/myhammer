const express = require('express')

const router = express.Router()

const jobsJson = require('../data/jobs')

router.get('/', (req, res) => {
  const jobs = jobsJson.body || []
  res.json(jobs)
})

router.get('/:id', (req, res) => {
  const jobs = jobsJson.body || []
  
  const job = jobs.find((job) => job.id === req.params.id)
  res.send(job || {})
})

module.exports = router
