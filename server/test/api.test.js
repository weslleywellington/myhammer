const request = require('supertest')

const app = require('../src/app')
const jobsJson = require('../src/data/jobs')

describe('GET /api/v1/jobs', () => {
  it('responds with a json message', (done) => {
    request(app)
      .get('/api/v1/jobs')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, jobsJson.body, done)
  })
})

describe(`GET /api/v1/jobs/${ jobsJson.body[0].id }`, () => {
  it('responds with a json message', (done) => {
    request(app)
      .get(`/api/v1/jobs/${ jobsJson.body[0].id }`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, jobsJson.body[0], done)
  })
})
